# CHANGELOG

<!--- next entry here -->

## 0.1.0
2020-04-10

### Features

- added Volume (43844b6b4dc7ff4acec07373437860eb9c528123)
- added Docker Hub export (52fce4a47d09c03f05f3ebe5467b4950ebd72f97)

### Fixes

- ci intendation (045016e10c8ba699bdf14b732b061621eedfb601)
- compose volumes (6f0a640cc7088945e80f6774c8b896c3b36be291)
- CI yml (b1b04771ef565ca9878a14ecfbf3d41b335b8958)
- variables syntax (430ce20b5cb622ec3a4713fee975f98816524e71)
- Registry Credentials (47be50328c868b43b936579d00aef0abd833e071)
- incorrect calculation for DOMAINS (5769ef3bfa6e5aef5f8539bc02e8964fe3ec0b82)

## 0.2.1
2020-03-30

### Fixes

- Registry Credentials (47be50328c868b43b936579d00aef0abd833e071)

## 0.2.0
2020-03-30

### Features

- added Docker Hub export (52fce4a47d09c03f05f3ebe5467b4950ebd72f97)

### Fixes

- CI yml (b1b04771ef565ca9878a14ecfbf3d41b335b8958)
- variables syntax (430ce20b5cb622ec3a4713fee975f98816524e71)

## 0.1.0
2020-03-30

### Features

- added Volume (43844b6b4dc7ff4acec07373437860eb9c528123)

### Fixes

- ci intendation (045016e10c8ba699bdf14b732b061621eedfb601)
- compose volumes (6f0a640cc7088945e80f6774c8b896c3b36be291)

## 0.0.1
2020-03-30

### Fixes

- ci intendation (045016e10c8ba699bdf14b732b061621eedfb601)