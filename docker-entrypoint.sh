#!/bin/bash
set -e

if [ "$1" = 'run' ]; then
    echo "Starting Certificate Automation"
    cd /acme.sh
    /usr/local/bin/acme.sh --install --force --cert-home /ssl --home /config --accountemail ${LE_EMAIL}
    cd /app
    while true; 
    do 
        ansible-playbook main.yml $( (( ${DEBUG:-0} == 1 )) && printf %s '-v' );
        echo "-------------------------------------------"
        echo "Sleeping for ${SLEEP:-3600}s";
        echo "-------------------------------------------"
        sleep ${SLEEP:-3600};
    done
fi

exec "$@"
