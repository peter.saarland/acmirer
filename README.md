# acmirer 
The ACME Acquirer, a central yet distributed Certificate Management Container that handles Issuing and automatic Renewal of SSL certificates using DNS-based challenges. See a list [compatible DNS servers](https://github.com/acmesh-official/acme.sh/wiki/dnsapi).

# Get Started
To validate your Requests, **acmirer** uses LetsEncrypt's DNS-Challenge and automatically sets and deletes the necessary records at your DNS Service of choice. To find the necessary configuration options for your DNS Service, please have a look at the list of [compatbible DNS servers](https://github.com/acmesh-official/acme.sh/wiki/dnsapi).

**acmirer** will save your certificates to `/ssl`. You should mount a volume to `/ssl` to persist certificates.

## Docker
Keep your settings in an `.env` file to ease of use or simply add them to the command manually.

```
source .env && docker run -d \
    -e DOMAINS=${DOMAINS} \
    -e MIAB_Username=${MIAB_Username} \
    -e MIAB_Password=${MIAB_Password} \
    -e MIAB_Server=${MIAB_Server} \
    -e LE_EMAIL=${LE_EMAIL}
    -v acmirer-ssl:/ssl \
    -v acmirer-config:/config \
    registry.gitlab.com/peter.saarland/acmirer:latest run`
```

## Docker Compose
See [docker-compose.yml](docker-compose.yml). This Compose File expects an `.env` file in place.

`docker-compose up -d`

# Maintenance

- List Certificates: `docker run -it --rm -v ansible-acme-ssl:/ssl ansible-acme acme.sh --list`

# Available Configuration

```

DOMAINS= # comma-separated list of Domains to acquire certificates for, e.g. *.example.com,example.com
STAGING=1 # LetsEncrypt staging-environment is activated by default, set to 0 for production certificates
DEBUG=0 # set to 1 for acme.sh debug output
FORCE_ISSUE=0 # set to 1 to force certificate issuance
FORCE_RENEW=0 # set to 1 to renew certificate even if renewal-date is not yet reached
DNS_PROVIDER=dns_miab # find list of available DNS Providers and necessary Environment 
CA_PROVIDER= 
Variables here: https://github.com/acmesh-official/acme.sh/wiki/dnsapi
DNS_SLEEP=120 # time to sleep in seconds before validation of _acme records with your DNS server
ISSUE_PRE_HOOK= # Command to be run before obtaining any certificates.
ISSUE_POST_HOOK= # Command to be run after attempting to obtain/renew certificates. No matter the obtain/renew is success or failed.
ISSUE_RENEW_HOOK= # Command to be run once for each successfully renewed certificate.
```

# Relevant Docs
- [Ansible](https://docs.ansible.com/)
- [acme.sh](https://github.com/acmesh-official/acme.sh/wiki)

# Local Development
```
docker build . -t acmirer
docker run -it --rm -e SLEEP=10 -e DOMAINS="*.example.com,example.com," -v ansible-acme-ssl:/ssl -v "${PWD}:/app" acmirer:latest ansible-playbook main.yml -v
```