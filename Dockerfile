FROM registry.gitlab.com/peter.saarland/ansible-boilerplate:latest
LABEL maintainer="Fabian Peter <fabian@peter.saarland>"

RUN git clone https://github.com/Neilpang/acme.sh.git /acme.sh \
    && cd /acme.sh \
    && ln -s /acme.sh/acme.sh /usr/local/bin
RUN mkdir -p /app /ssl /config

WORKDIR /app
COPY . .
RUN echo 'Ansible refuses to read from a world-writeable folder, hence' \
    && chmod -v 700 $(pwd)

VOLUME /ssl
ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["run"]
ARG BUILD_DATE
LABEL org.label-schema.build-date=$BUILD_DATE